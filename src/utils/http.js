import axios from 'axios'
import { useUserStore } from '@/stores/user';
const instance = axios.create({
    baseURL: 'http://localhost:8080',
    //超时时间
    timeout: 5000
  });
// Add a request interceptor
instance.interceptors.request.use(function (config) {
    const userstore = useUserStore()
    config.headers['token'] = userstore.userinfo.token
    return config;
  }, function (error) {
    return Promise.reject(error);
  });

// Add a response interceptor
instance.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    return Promise.reject(error);
  });
export default instance