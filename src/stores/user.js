import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useUserStore = defineStore('user',()=>{
    const userinfo = ref({
        userid: '',
        token: ''
    })
    const setuserid = (userid) => {
        userinfo.value.userid = userid
    }
    return {
        userinfo,
        setuserid
    }
})