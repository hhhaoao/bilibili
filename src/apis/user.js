import instance from "@/utils/http";
export const logincheck = ({phone,password}) => {
    return instance({
        url: '/user/login',
        method: 'post',
        data: {
            phone,
            password
        }
    })
}